window.addEventListener('load', () => {
	const sidebar = document.querySelector('.sidebar'),
		fixSidebar = document.querySelector('.sidebar__logo'),
		logoImg = document.querySelector('.sidebar-logo__img'),
		logoArrow = document.querySelector('.logo__arrow-right'),
		notifications = document.querySelectorAll('.notification'),
		paddingBlock = document.querySelector('.dashboard__content'),
		sidebarPopups = document.querySelectorAll('.sidebar__popup'),
		navCategoryLink = document.querySelectorAll('.navigation__category'),
		warehouseBtns = document.querySelectorAll('.table__top-btn'),
		ProductsBtns = document.querySelectorAll('.table__products-change-btn'),
		tableInner = document.querySelector('.table__inner'),
		outputWrapper = document.querySelector('.tbody'),
		allCheckbox = document.querySelector('.allCheckbox'),
		wrapper = document.querySelector('.settings-modal'),
		btnAllProductsTable = document.getElementById('allTableProducts'),
		btnSaleProductsTable = document.getElementById('saleTableProducts'),
		btnNoSaleProductsTable = document.getElementById('noSaleTbaleProducts'),
		archiveProductsTable = document.getElementById('archiveTableProducts'),
		outputCtgList = document.getElementById('outputCtg'),
		btnLockLeftPanel = document.getElementById('lockLeftPanel'),
		outputTreeContainer = document.querySelector('.categoryPanel__tree'),
		createSinonimBtn = document.getElementById('createSinonim'),
		createSinonimInput = document.getElementById('createSinonimInput'),
		outputSinonims = document.querySelector('.outputSinonims');
	// переменная для счетчика чекбоксов
	let count = 0,
		count2 = 0,
		columnList = [];
	// массив карточек товара
	const allCardProducts = [
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Не продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Архив',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Архив',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Архив',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Архив',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
		{
			index: 'table-input-',
			article: 'RK0498394',
			picture: '../img/tableTestPhoto.png',
			name: 'Выключатель зажигания для Лада Калина 1117, 1118, 1119 комплект с личинками (иммобилайзер активен)',
			status: 'Продается',
			price: 3000,
			remains: 5,
			pn: '2170-7903010',
			barcode: '4606265 334567',
			usability: 'Lada Granta 2170, 2171, 2172 Renault Logan 2020 FL',
		},
	];
	// функция для выгрузки карточек товара
	function outputTracks(arr) {
		for (let i = 0; i < arr.length; i++) {
			// выгрузка всех карточек товара в таблицу
			// переменная, которая будет использоваться для выгрузки карточек товара
			let outputCardsProducts =
				`
			<tr class="tbody__item">
				<td class="article">
					<input class="article__checkbox" type="checkbox" id="${arr[i].index + (i + 1)}"
						name="${arr[i].index + (i + 1)}">
					<label class="table__lable" for="${arr[i].index + (i + 1)}">
						<span>${arr[i].article}</span>
					</label>
				</td>
				<td class="picture">
					<img class="picture__img" src="${arr[i].picture}" alt="#">
				</td>
				<td class="name">
					<a class="name__title" href="##">
						${arr[i].name}
					</a>
				</td>
				<td class="status">
					<span class="status__text">
						${arr[i].status}
					</span>
				</td>
				<td class="price">
					<span class="price__text">
						${arr[i].price}р
					</span>
				</td>
				<td class="remains">
					<span class="remains__text">
						${arr[i].remains}
					</span>
				</td>
				<td class="pn">
					<span class="pn__text">
						${arr[i].pn}
					</span>
				</td>
				<td class="barcode">
					<span class="barcode__text">
						${arr[i].barcode}
					</span>
				</td>
				<td class="usability">
					<span class="usability__text">
						${arr[i].usability}
					</span>
				</td>
				<td class="settings settings__product">
					<button class="settings__button" type="button">

					</button>
				</td>
			</tr>
		`;
			outputWrapper.insertAdjacentHTML("beforeend", outputCardsProducts);
		};
		checkboxCheckedCount();
		openModalSettings();
	};
	// добавление всем кнопкам склада дата атрибуты, чтобы было удобно найти нужную кнопку при перезагрузке страницы и добавить ей класс
	for (let i = 0; i < warehouseBtns.length; i++) {
		warehouseBtns[i].setAttribute('data-count', i);
	};
	// добавление всем кнопкам фильтра товаров дата атрибуты, чтобы было удобно найти нужную кнопку при перезагрузке страницы и добавить ей класс
	for (let i = 0; i < ProductsBtns.length; i++) {
		ProductsBtns[i].setAttribute('data-count', i);
	};
	fixPadding();
	if (outputWrapper != null || outputWrapper != undefined) {
		outputTracks(allCardProducts);
	};
	if (tableInner != null) {
		scrollLeftShadow();
	};
	// раскрытый сайдбар по умолчанию
	if (localStorage.getItem('fixSidebar') != 'false') {
		sidebar.classList.add('fixation', 'no-animation');
		logoImg.classList.add('fixation', 'no-animation');
		logoArrow.classList.add('fixation', 'no-animation');
		paddingBlock.classList.add('no-animation');
		localStorage.setItem('fixSidebar', true);
	};
	reloadPageCheck();
	function reloadPageCheck() {
		if (localStorage.getItem('data-list') == null || localStorage.getItem('data-list') == undefined) {
			for (let i = 0; i < document.querySelectorAll('.settings-modal__label input[type="checkbox"]').length; i++) {
				document.querySelectorAll('.settings-modal__label input[type="checkbox"]')[i].checked = true;
			};
		} else if (JSON.parse(JSON.stringify(localStorage.getItem('data-list'))).length > 0) {
			const makeUniq = (arr) => {
				return arr.filter((el, id) => arr.indexOf(el) === id);
			};
			let array = [];
			for (let i = 0; i < document.querySelectorAll('th').length; i++) {
				array.push(document.querySelectorAll('th')[i].className);
				makeUniq(array);
			};
			for (let i = 0; i < array.length; i++) {
				for (let j = 0; j < JSON.parse((localStorage.getItem('data-list'))).length; j++) {
					if (array[i] == JSON.parse((localStorage.getItem('data-list')))[j]) {
						deleteColumn(document.querySelectorAll(`.${array[i]}`));
					};
				};
			};
		};
	};
	// добавление класса активной категории, на которой находится пользователь
	for (let i = 0; i < navCategoryLink.length; i++) {
		// проверка условия, где берется ссылка текущей страницы, обрезается и если название файла страницы
		// совпадает с названием id кнопки категории, то добавляется класс
		if (window.location.pathname.split('/').pop().replace(/\.[^/.]+$/, "") === navCategoryLink[i].id) {
			navCategoryLink[i].classList.add('active');
		};
	};
	// проверка на наличие порядкового номера нажатой кнопки склада
	if (localStorage.getItem('activeWarehouse') != null || localStorage.getItem('activeWarehouse') != undefined) {
		// если нашли, то снимаем класс со всех
		for (let i = 0; i < warehouseBtns.length; i++) {
			warehouseBtns[i].classList.remove('active');
		};
		// дальше получаем номер этой кнопки и добавляем ей класс
		let count = localStorage.getItem('activeWarehouse');
		if (document.querySelector(".table__top-btn[data-count='" + count + "']") != null) {
			document.querySelector(".table__top-btn[data-count='" + count + "']").classList.add('active');
		};
		// кнопки сортировки товаров в продаже
	};
	if (localStorage.getItem('activeProduct') != null || localStorage.getItem('activeProduct') != undefined) {
		// если нашли, то снимаем класс со всех
		for (let i = 0; i < ProductsBtns.length; i++) {
			ProductsBtns[i].classList.remove('active');
		};
		// дальше получаем номер этой кнопки и добавляем ей класс
		let count2 = localStorage.getItem('activeProduct');
		if (document.querySelector(".table__products-change-btn[data-count='" + count2 + "']") != null) {
			document.querySelector(".table__products-change-btn[data-count='" + count2 + "']").classList.add('active');
		} else if (count2 == null) {
			allProductsFilter();
		};
	};
	if (btnAllProductsTable != null) {
		btnAllProductsTable.nextElementSibling.textContent = allCardProducts.length;
	};
	visibleNotification(btnSaleProductsTable, allCardProducts, 'Продается');
	visibleNotification(btnNoSaleProductsTable, allCardProducts, 'Не продается');
	visibleNotification(archiveProductsTable, allCardProducts, 'Архив');
	notificationText();
	if (btnSaleProductsTable != null || btnNoSaleProductsTable != null || archiveProductsTable != null) {
		saleProductsFilter();
		btnNoSaleProductsFilter();
		archiveProductsFilter();
	};
	// обработчик клика фиксации сайдбара
	fixSidebar.addEventListener('click', fixationSidebar);
	// обработчики события мышки для правильного отступа слева, зависящего от размера сайдбара
	sidebar.addEventListener('mouseover', fixPadding);
	sidebar.addEventListener('mouseout', fixPadding);
	sidebar.addEventListener('mousemove', fixPadding);
	// функция для правильного опрделения отступа слева
	function fixPadding() {
		setInterval(() => {
			paddingBlock.style.paddingLeft = sidebar.offsetWidth + 20 + 'px';
			autoLeftSidebarPopup();
			if (tableInner != null) {
				scrollLeftShadow();
			};
		}, 0);
	};
	// автоматический расчет появления внутреннего меню при наведении мыши на категории в сайдбаре
	function autoLeftSidebarPopup() {
		sidebarPopups.forEach(sidebarPopup => {
			sidebarPopup.style.left = sidebar.offsetWidth + 'px';
		});
	};
	// обработчик кликов по кнопкам склада
	warehouseBtns.forEach(warehouseBtn => {
		warehouseBtn.addEventListener('click', changeWarehouse);
	});
	// обработчик кликов по кнопкам товаров
	ProductsBtns.forEach(ProductsBtn => {
		ProductsBtn.addEventListener('click', filterProducts);
	});
	// обработчики кликов фильтрации
	if (btnSaleProductsTable != null || btnNoSaleProductsTable != null || archiveProductsTable != null || btnAllProductsTable != null) {
		btnAllProductsTable.addEventListener('click', allProductsFilter);
		btnSaleProductsTable.addEventListener('click', saleProductsFilter);
		btnNoSaleProductsTable.addEventListener('click', btnNoSaleProductsFilter);
		archiveProductsTable.addEventListener('click', archiveProductsFilter);
	};
	// обработчик внутреннего скролла в таблице для добавления тениХ
	if (tableInner != null) {
		tableInner.addEventListener('scroll', scrollLeftShadow);
	};
	// обработчик события выделения всех чекбоксов карточек товара
	if (allCheckbox != null) {
		allCheckbox.addEventListener('click', selectAllCheckbox);
	};
	// функция для фиксации сайдбара
	function fixationSidebar() {
		if (!sidebar.classList.contains('fixation')) {
			sidebar.classList.add('fixation');
			logoImg.classList.add('fixation');
			logoArrow.classList.add('fixation');
			localStorage.setItem('fixSidebar', true);
			fixPadding();
			autoLeftSidebarPopup();
		} else {
			sidebar.classList.remove('fixation', 'no-animation');
			logoImg.classList.remove('fixation', 'no-animation');
			logoArrow.classList.remove('fixation', 'no-animation');
			localStorage.setItem('fixSidebar', false);
			fixPadding();
			autoLeftSidebarPopup();
		}
	};
	// функция для изменения количества сообщений если оно превышает 100
	function notificationText() {
		notifications.forEach(notification => {
			if (notification.innerText >= 100) {
				notification.innerText = '+99';
			}
		});
	};
	// функция для запоминания и изменения стиля активной кнопки склада
	function changeWarehouse() {
		for (let i = 0; i < warehouseBtns.length; i++) {
			warehouseBtns[i].classList.remove('active');
		};
		this.classList.add('active');
		localStorage.setItem('activeWarehouse', this.dataset.count);
	};
	// функция для запоминания фильтра товаров
	function filterProducts() {
		for (let i = 0; i < ProductsBtns.length; i++) {
			ProductsBtns[i].classList.remove('active');
		};
		this.classList.add('active');
		localStorage.setItem('activeProduct', this.dataset.count);
	};
	// функция для добавления тени артиклю и настройкам в таблице
	function scrollLeftShadow() {
		const article = document.querySelectorAll('.article'),
			settings = document.querySelectorAll('.settings');
		let x = tableInner.scrollLeft,
			maxScrollLeft = tableInner.scrollWidth - tableInner.clientWidth;
		if (tableInner.scrollWidth != tableInner.offsetWidth) {
			if (x >= 1) {
				for (let i = 0; i < article.length; i++) {
					article[i].classList.add('shadow');
				};
				for (let i = 0; i < settings.length; i++) {
					settings[i].classList.add('shadow');
				};
			}
			else {
				for (let i = 0; i < article.length; i++) {
					article[i].classList.remove('shadow');
				};
				for (let i = 0; i < settings.length; i++) {
					settings[i].classList.add('shadow');
				};
			}
			if (x === maxScrollLeft) {
				for (let i = 0; i < settings.length; i++) {
					settings[i].classList.remove('shadow');
				};
			}
		}
	};
	// функция для выделения всех чекбоксов
	function selectAllCheckbox() {
		const allProductsCheckbox = document.querySelectorAll('.article__checkbox');
		if (this.checked) {
			for (let i = 0; i < allProductsCheckbox.length; i++) {
				count = allProductsCheckbox.length;
				allProductsCheckbox[i].checked = true;
				document.querySelector('.table__products-btn').innerText = 'Массовое действие (' + count + ')';
			}
		} else {
			count = 0;
			for (let i = 0; i < allProductsCheckbox.length; i++) {
				allProductsCheckbox[i].checked = false;
				document.querySelector('.table__products-btn').innerText = 'Массовое действие';
			}
		}
	};
	// функция для добавления счетчика выделенных чекбоксов
	function checkboxCheckedCount() {
		const allProductsCheckbox = document.querySelectorAll('.article__checkbox');
		for (let i = 0; i < allProductsCheckbox.length; i++) {
			allProductsCheckbox[i].addEventListener('change', () => {
				if (allProductsCheckbox[i].checked) {
					count++;
					document.querySelector('.table__products-btn').innerText = 'Массовое действие (' + count + ')';
					if (count === allProductsCheckbox.length) {
						allCheckbox.checked = true;
					}
				} else if (!allProductsCheckbox[i].checked) {
					count--;
					document.querySelector('.table__products-btn').innerText = 'Массовое действие (' + count + ')';
					if (count <= 0) {
						document.querySelector('.table__products-btn').innerText = 'Массовое действие';
					} else if (allCheckbox.checked && count <= allProductsCheckbox.length - 1) {
						allCheckbox.checked = false;
					}
				}
			});
		}
	};
	// открытие меню редактирования карточки товара
	function openModalSettings() {
		const settings = document.querySelectorAll('.settings');
		let count = 0, target1, target2;
		settings.forEach((el, index) => {
			el.addEventListener('click', (e) => {
				e.stopPropagation();
				let outputTag = `
					<button class="settings-modal__btn" type="button" id="edit">Редактировать</button>
					<button class="settings-modal__btn" type="button" id="copy">Копировать</button>
					<button class="settings-modal__btn" type="button" id="archive">Перенести в архив</button>
					<button class="settings-modal__btn" type="button" id="delete">Удалить</button>
				`;
				count++;
				if (count === 1) {
					target1 = index;
				};
				if (count === 2) {
					target2 = index;
				};
				if (count === 2 && target1 !== target2) {
					count = 0;
				};
				wrapper.classList.add('active');
				if (wrapper.classList.contains('active')) {
					wrapper.innerHTML = '';
					wrapper.insertAdjacentHTML('beforeend', outputTag);
				};
				if (e.target === document.querySelector('.edit-table') || e.target === document.querySelector('.edit-table').parentElement) {
					let outputTag = `
						<h class="settings-modal__title">
							Настройки
						</h>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editArticle">
							<input type="checkbox">
							<span>Артикул</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editPhoto">
							<input type="checkbox">
							<span>Фото</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editName">
							<input type="checkbox">
							<span>Название</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editStatus">
							<input type="checkbox">
							<span>Статус</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editPrice">
							<input type="checkbox">
							<span>Цена</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editRemains">
							<input type="checkbox">
							<span>Остаток</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editPN">
							<input type="checkbox">
							<span>PN Запчасти</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editBarcode">
							<input type="checkbox">
							<span>Штрихкод</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
						<label class="settings-modal__btn settings-modal__label settings-modal__label-checked" id="editUsability">
							<input type="checkbox">
							<span>Применимость</span>
							<div class="settings-modal__custom-checkbox"></div>
						</label>
					`;
					wrapper.innerHTML = '';
					wrapper.insertAdjacentHTML('beforeend', outputTag);
					openCheckCheckboxAndColumn();
				};
				X = el.getBoundingClientRect().right - wrapper.offsetWidth + 5;
				Y = el.getBoundingClientRect().bottom + window.scrollY + 5;
				if (el.getBoundingClientRect().top > tableInner.clientHeight) {
					Y = el.getBoundingClientRect().top - wrapper.offsetHeight - 5 + window.scrollY;
				};
				if (tableInner.clientHeight < 200) {
					Y = el.getBoundingClientRect().bottom + window.scrollY + 5;
				};
				wrapper.style.transform = 'translate3d(' + X + 'px,' + Y + 'px,' + '0px)';
				if (target1 === target2) {
					wrapper.classList.remove('active');
					wrapper.innerHTML = '';
					target1 = undefined;
					target2 = undefined;
					count = 0;
				};
			});
			// обработка кликов, если они сделаны не по кнопке
			document.addEventListener('click', (e) => {
				if (e.target !== el && wrapper.classList.contains('active') && !wrapper.contains(e.target)) {
					wrapper.classList.remove('active');
					wrapper.innerHTML = '';
					count = 0;
					target1 = undefined;
					target2 = undefined;
				};
			});
		});
	};
	deleteColumnTable();
	function openCheckCheckboxAndColumn() {
		if (localStorage.getItem('data-list') == null || localStorage.getItem('data-list') == undefined) {
			for (let i = 0; i < document.querySelectorAll('.settings-modal__label input[type="checkbox"]').length; i++) {
				document.querySelectorAll('.settings-modal__label input[type="checkbox"]')[i].checked = true;
			};
		} else if (JSON.parse(JSON.stringify(localStorage.getItem('data-list'))).length > 0) {
			for (let i = 0; i < document.querySelectorAll('.settings-modal__label input[type="checkbox"]').length; i++) {
				document.querySelectorAll('.settings-modal__label input[type="checkbox"]')[i].checked = true;
			};
			for (let i = 0; i < document.querySelectorAll('.settings-modal__label').length; i++) {
				for (let j = 0; j < JSON.parse((localStorage.getItem('data-list'))).length; j++) {
					if (document.querySelectorAll('.settings-modal__label')[i].getAttribute('id') == JSON.parse((localStorage.getItem('data-list')))[j]) {
						document.querySelectorAll('.settings-modal__label')[i].querySelector('input').checked = false;
						document.querySelectorAll('.settings-modal__label')[i].classList.remove('settings-modal__label-checked');
					};
				};
			};
		};
	};
	// функция для удаления столбца
	function deleteColumn(column) {
		for (let i = 0; i < column.length; i++) {
			column[i].style.display = 'none';
		};
	};
	// фукнция для возвращения колонки
	function resetColumn(column) {
		for (let i = 0; i < column.length; i++) {
			column[i].style.display = '';
		};
	};
	if (localStorage.getItem('data-list') != null || localStorage.getItem('data-list') != undefined) {
		for (let i = 0; i < JSON.parse((localStorage.getItem('data-list'))).length; i++) {
			columnList.push(JSON.parse((localStorage.getItem('data-list')))[i]);
		};
	};
	// удаление столбцов в таблице
	function deleteColumnTable() {
		// кнопки для удаления столбцов в таблице
		function checkTargetButton(target, button, column) {
			if (target === button && button.classList.contains('settings-modal__label-checked')) {
				button.classList.remove('settings-modal__label-checked');
				deleteColumn(column);
				columnList.push(button.getAttribute('id'));
				columnList.push(column[0].className);
				localStorage.setItem('data-list', JSON.stringify(columnList));
			} else if (target === button && !button.classList.contains('settings-modal__label-checked')) {
				// 
				button.classList.add('settings-modal__label-checked');
				let myIndex = columnList.indexOf(button.getAttribute('id'));
				if (myIndex !== -1) {
					columnList.splice(myIndex, 1);
					let myIndex2 = columnList.indexOf(column[0].className);
					if (myIndex2 !== -1) {
						columnList.splice(myIndex2, 1);
					}
				}
				localStorage.setItem('data-list', JSON.stringify(columnList));
				resetColumn(column);
			};
		};
		// клики по нужным кнопкам
		document.addEventListener('click', (e) => {
			const editArticle = document.getElementById('editArticle'),
				editPhoto = document.getElementById('editPhoto'),
				editName = document.getElementById('editName'),
				editStatus = document.getElementById('editStatus'),
				editPrice = document.getElementById('editPrice'),
				editRemains = document.getElementById('editRemains'),
				editPN = document.getElementById('editPN'),
				editBarcode = document.getElementById('editBarcode'),
				editUsability = document.getElementById('editUsability'),
				allArticles = document.querySelectorAll('.article'),
				allPicture = document.querySelectorAll('.picture'),
				allName = document.querySelectorAll('.name'),
				allStatus = document.querySelectorAll('.status'),
				allPrice = document.querySelectorAll('.price'),
				allRemains = document.querySelectorAll('.remains'),
				allPn = document.querySelectorAll('.pn'),
				allBarcode = document.querySelectorAll('.barcode'),
				allUsability = document.querySelectorAll('.usability');
			let targets = e.target;
			checkTargetButton(targets, editArticle, allArticles);
			checkTargetButton(targets, editPhoto, allPicture);
			checkTargetButton(targets, editName, allName);
			checkTargetButton(targets, editStatus, allStatus);
			checkTargetButton(targets, editPrice, allPrice);
			checkTargetButton(targets, editRemains, allRemains);
			checkTargetButton(targets, editPN, allPn);
			checkTargetButton(targets, editBarcode, allBarcode);
			checkTargetButton(targets, editUsability, allUsability);
		});
	};
	// фильтр для кнопки "все товары"
	function allProductsFilter(e) {
		if (document.querySelectorAll('.tbody__item').length === allCardProducts.length && btnAllProductsTable.classList.contains('active') && btnAllProductsTable != null) {
			e.stopPropagation();
		} else if (btnAllProductsTable.classList.contains('active') && btnAllProductsTable != null) {
			outputWrapper.innerHTML = '';
			outputTracks(allCardProducts);
			allCheckbox.checked = false;
			reloadPageCheck();
		};
	}
	// фильтр для кнопки "в продаже"
	function saleProductsFilter(e) {
		if (document.querySelectorAll('.tbody__item').length === allCardProducts.filter(el => el.status === 'Продается').length && btnSaleProductsTable.classList.contains('active') && btnSaleProductsTable != null) {
			e.stopPropagation();
		} else if (btnSaleProductsTable.classList.contains('active') && btnSaleProductsTable != null) {
			outputWrapper.innerHTML = '';
			outputTracks(allCardProducts.filter(el => el.status === 'Продается'));
			btnSaleProductsTable.nextElementSibling.textContent = allCardProducts.filter(el => el.status === 'Продается').length;
			allCheckbox.checked = false;
			reloadPageCheck();
		};
	};
	// фильтр для кнопки "не в продаже"
	function btnNoSaleProductsFilter(e) {
		if (document.querySelectorAll('.tbody__item').length === allCardProducts.filter(el => el.status === 'Не продается').length && btnNoSaleProductsTable.classList.contains('active') && btnNoSaleProductsTable != null) {
			e.stopPropagation();
		} else if (btnNoSaleProductsTable.classList.contains('active') && btnNoSaleProductsTable != null) {
			outputWrapper.innerHTML = '';
			outputTracks(allCardProducts.filter(el => el.status === 'Не продается'));
			allCheckbox.checked = false;
			reloadPageCheck();
		};
	};
	// фильтр для кнопки "архив"
	function archiveProductsFilter(e) {
		if (document.querySelectorAll('.tbody__item').length === allCardProducts.filter(el => el.status === 'Архив').length && archiveProductsTable.classList.contains('active') && archiveProductsTable != null) {
			e.stopPropagation();
		} else if (archiveProductsTable.classList.contains('active') && archiveProductsTable != null) {
			outputWrapper.innerHTML = '';
			outputTracks(allCardProducts.filter(el => el.status === 'Архив'));
			allCheckbox.checked = false;
			reloadPageCheck();
		};
	};
	// функция для показа количества карточек товара в нотифире
	function visibleNotification(element, arr, status) {
		if (element != null) {
			element.nextElementSibling.textContent = arr.filter(el => el.status === `${status}`).length;
		};
	};
	// выгрузка списков категорий и их внутренностей
	if (outputCtgList) {
		let id = 1,
			ctgArray,
			ctgSelected = null;

		function generateList(level) {
			var i, n, n1, n2, name, ctgArray = {};
			if (level == 1) n1 = n2 = 10, name = "Вид техники";
			else if (level == 2) n1 = 10, n2 = 25, name = "Категория";
			else if (level == 3) n1 = 5, n2 = 20, name = "Подкатегория";
			else if (level == 4) n1 = 20, n2 = 30, name = "Группа";
			else if (level == 5) n1 = 10, n2 = 40, name = "Название";
			n = n1 + Math.floor(Math.random() * (n2 - n1));
			for (i = 0; i < n; i++) {
				var id0 = id++;
				ctgArray[id0] = { name: name + " " + (i + 1) };
				if (level < 5) ctgArray[id0].sub = generateList(level + 1);
			};
			return ctgArray;
		};
		ctgArray = generateList(1);
		function onClickCtgList(ev) {
			var target = ev.target;
			var data, e;
			if (!target.dataset.ids) return; // это не элемент дерева
			e = document.getElementById(target.id + "-c");
			if (e) { // если уже создан дочерний блок
				if (target.dataset.sel !== '1') {
					target.dataset.sel = '1';
					e.style.display = 'block';
				} else {
					e.style.display = e.style.display == "none" ? "block" : "none";
				}
			} else { // создаю дочерний блок со всеми элементами
				data = findDataByIds(ctgArray, ev.target.dataset.ids);
				if (!data) return;
				buldChildren(target, data);
			}
			if (ctgSelected) {
				ctgSelected.dataset.sel = 0;
			};
			ctgSelected = target;
			target.dataset.sel = 1;
			// дерево пути
			generateCard(target);
			parentsCount(target);
		};
		const accordionsAndProperty = [
			{
				lvl: '1',
				title: 'Свойства',
				accordions: [
					{
						title: 'Состояние',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
					{
						title: 'Бренд',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
				],
			},
			{
				lvl: '2',
				title: 'Свойства',
				accordions: [
					{
						title: 'Состояние2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
					{
						title: 'Бренд2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
				],
			},
			{
				lvl: '3',
				title: 'Свойства',
				accordions: [
					{
						title: 'Состояние2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
					{
						title: 'Бренд2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
				],
			},
			{
				lvl: '4',
				title: 'Свойства',
				accordions: [
					{
						title: 'Состояние2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
					{
						title: 'Бренд2',
						accordionInner: [
							{
								name: 'Тестовое название',
								comment: 'Тестовый коммент',
							},
							{
								name: 'Тестовое название2',
								comment: 'Тестовый коммент2',
							},
						],
					},
				],
			},
			{
				lvl: '5',
				title: '',
			},
		];
		// **************
		function getChildNameComment(accordionInner) {
			let accordionItems = ``;
			accordionInner.forEach(({ name, comment }) => {
				accordionItems += `
					<li class="accordion__list-item">
						<span class="accordion__name">${name}</span>
						<p class="accordion__comment">${comment}</p>
					</li>
				`;
			});
			return accordionItems;
		}
		function getChild(accordions) {
			let accordion = ``;
			accordions.forEach(({ title, accordionInner }) => {
				accordion += `
						<div class="accordion">
							<div class="accordion__wrapper">
								<div class="accordion__draggable">
									<img src="../img/icons/draggableAccordion.svg" alt="#" draggable="false">
								</div>
								<div class="accordion__target">
									<h4 class="accordion__title">${title}</h4>
									<div class="dropdown">
										<div class="select" data-placeholder="Способ выбора">
											<span class="selected">Выпадающий список</span>
											<img class="caret" src="../img/icons/accordionCaret.svg" alt="#" draggable="false">
										</div>
										<ul class="menu">
											<li>
												Выпадающий список
											</li>
											<li>
												Радиокнопка
											</li>
											<li>
												Чекбокс
											</li>
											<li>
												Поле свободного ввода
											</li>
										</ul>
									</div>
									<div class="dropdown">
										<div class="select" data-placeholder="Сортировка">
											<span class="selected"></span>
											<img class="caret" src="../img/icons/accordionCaret.svg" alt="#" draggable="false">
										</div>
										<ul class="menu">
											<li>
												a-Z
											</li>
											<li>
												Z-a
											</li>
											<li>
												По популярности
											</li>
											<li>
												В ручную
											</li>
										</ul>
									</div>
									<div class="dropdown">
										<div class="select" data-placeholder="Единицы">
											<span class="selected">шт</span>
											<img class="caret" src="../img/icons/accordionCaret.svg" alt="#" draggable="false">
										</div>
										<ul class="menu">
											<li>
												шт
											</li>
										</ul>
									</div>
									<div class="accordion__menu">
										<img src="../img/icons/accrodionMenu.svg" alt="#" draggable="false">
									</div>
								</div>
								<div class="accordion__decompression">
									<img class="accordion__decompression-img-hidden" src="../img/icons/decompression.svg" alt="#" draggable="false">
									<img class="accordion__decompression-img-active" src="../img/icons/decompressionActive.svg" alt="#" draggable="false">
								</div>
							</div>
							<div class="accordion__inner">
								<div class="accordion__inner-content">
									<ul class="accordion__list">
										${getChildNameComment(accordionInner)}
									</ul>
									<div class="creates">
										<input class="creates__input" type="text">
										<button class="creates__button" type="button">Создать</button>
										<div class="message">
											help text
										</div>
									</div>
								</div>
							</div>
						</div>
				`;
			});
			return accordion;
		}
		function generateCard(target) {
			document.getElementById('categoryPanel__edit-content-output').innerHTML = '';
			let innerTitle = ``,
				count = 0;
			accordionsAndProperty.forEach(({ title, accordions, lvl }) => {
				let div = document.createElement('div');
				div.classList.add('categoryPanel__edit-card');
				div.dataset.lvl = lvl;
				count++;
				if (count > parseInt(target.dataset.level)) {
					return false;
				}
				innerTitle = `
					<h3 class="categoryPanel__edit-card-title">${title}</h3>
				`;
				let readyMadeCard = `

				`;
				if (lvl == '5') {
					readyMadeCard = `
					<h3 class="categoryPanel__edit-card-title categoryPanel__name-title">
						${target.innerText}
					<button class="creates__button name-change" type="button">Изменить</button>
					</h3>
					<div class="name__wrapper">
						<div class="name__img-output">
							<div class="name__main-output-img">
								<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
							</div>
							<div class="name__scrollbar-img">
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
								<div class="name__main-scrollbar-img">
									<img class="name__main-img" src="../img/cardSlider-1.webp" alt="" draggable="false">
								</div>
							</div>
						</div>
						<div class="name__related">
							<h3 class="name__related-title">
								Связанные товары
							</h3>
							<div class="cards">
								<div class="card">
									<button class="favourites">
										<img class="favorites-img" src="../img/icons/favorites.svg" alt="">
									</button>
									<a class="card-link" href="#">
										<img class="card-img" src="../img/products/sale1.webp" alt="" draggable="false">
									</a>
									<div class="card__wrapper">
										<div class="card__info">
											<h3 class="card-title">
												<a class="card-title-link" href="#">
													<span>Утка в шлеме / Утка в очках / Утка с пропеллером / Игрушка на
														панель
														авто / Подарок / Игрушка в машину на присосках / Тик Ток Уточка
														(Цыпленок)</span>
												</a>
											</h3>
										</div>
										<div class="price__wrapper">
											<span class="card-price">
												299 ₽
											</span>
										</div>
									</div>
								</div>
								<div class="card">
									<button class="favourites">
										<img class="favorites-img" src="../img/icons/favorites.svg" alt="">
									</button>
									<a class="card-link" href="#">
										<img class="card-img" src="../img/products/sale1.webp" alt="" draggable="false">
									</a>
									<div class="card__wrapper">
										<div class="card__info">
											<h3 class="card-title">
												<a class="card-title-link" href="#">
													<span>Утка в шлеме / Утка в очках / Утка с пропеллером / Игрушка на
														панель
														авто / Подарок / Игрушка в машину на присосках / Тик Ток Уточка
														(Цыпленок)</span>
												</a>
											</h3>
										</div>
										<div class="price__wrapper">
											<span class="card-price">
												299 ₽
											</span>
										</div>
									</div>
								</div>
								<div class="card">
									<button class="favourites">
										<img class="favorites-img" src="../img/icons/favorites.svg" alt="">
									</button>
									<a class="card-link" href="#">
										<img class="card-img" src="../img/products/sale1.webp" alt="" draggable="false">
									</a>
									<div class="card__wrapper">
										<div class="card__info">
											<h3 class="card-title">
												<a class="card-title-link" href="#">
													<span>Утка в шлеме / Утка в очках / Утка с пропеллером / Игрушка на
														панель
														авто / Подарок / Игрушка в машину на присосках / Тик Ток Уточка
														(Цыпленок)</span>
												</a>
											</h3>
										</div>
										<div class="price__wrapper">
											<span class="card-price">
												299 ₽
											</span>
										</div>
									</div>
								</div>
								<div class="card">
									<button class="favourites">
										<img class="favorites-img" src="../img/icons/favorites.svg" alt="">
									</button>
									<a class="card-link" href="#">
										<img class="card-img" src="../img/products/sale1.webp" alt="" draggable="false">
									</a>
									<div class="card__wrapper">
										<div class="card__info">
											<h3 class="card-title">
												<a class="card-title-link" href="#">
													<span>Утка в шлеме / Утка в очках / Утка с пропеллером / Игрушка на
														панель
														авто / Подарок / Игрушка в машину на присосках / Тик Ток Уточка
														(Цыпленок)</span>
												</a>
											</h3>
										</div>
										<div class="price__wrapper">
											<span class="card-price">
												299 ₽
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					`;
					div.insertAdjacentHTML('afterbegin', readyMadeCard);
					document.getElementById('categoryPanel__edit-content-output').insertAdjacentElement('afterbegin', div);
				} else {
					readyMadeCard = `
					${innerTitle}
					<div class="draggable__content">
						${getChild(accordions)}
					</div>
				`;
					div.insertAdjacentHTML('afterbegin', readyMadeCard);
					document.getElementById('categoryPanel__edit-content-output').insertAdjacentElement('beforeend', div);
				}
				div.scrollIntoView({ block: "center", behavior: "smooth" });
			});
			if (target.dataset.level != '5') {
				for (let i = 0; i < document.getElementById('categoryPanel__edit-content-output').children.length; i++) {
					document.getElementById('categoryPanel__edit-content-output').children[i].querySelector('.draggable__content').style.opacity = '0.6';
				}
				document.getElementById('categoryPanel__edit-content-output').lastElementChild.querySelector('.draggable__content').style.opacity = '1';
			} else {
				for (let i = 0; i < document.getElementById('categoryPanel__edit-content-output').children.length; i++) {
					if (document.getElementById('categoryPanel__edit-content-output').children[i].querySelector('.draggable__content')) {
						document.getElementById('categoryPanel__edit-content-output').children[i].querySelector('.draggable__content').style.opacity = '0.6';
					}

				}
			}
			accordionsFunctional();
		};
		// нахождение предыдущих элементов активной кнопки (дерево активного кликнутого элемента)
		function parentsCount(el) {
			if (!el) return 0;
			let array = [],
				activeTarget = el;

			for (let i = 0; el.parentElement; i++, el = el.parentElement.previousSibling) {
				if (el.className == 'categoryPanel__list') {
					return;
				} else {
					if (el.className == 'ctgList-itemContainer') {
						// Добавление заголовков в правую часть
						if (el.dataset.level < 5) {
							for (let i = 0; i < document.querySelectorAll('.categoryPanel__edit-card').length; i++) {
								if (el.dataset.level == document.querySelectorAll('.categoryPanel__edit-card')[i].dataset.lvl) {
									document.querySelectorAll('.categoryPanel__edit-card')[i].querySelector('.categoryPanel__edit-card-title').innerText = document.querySelectorAll('.categoryPanel__edit-card')[i].querySelector('.categoryPanel__edit-card-title').innerText + ' ' + el.innerText;
								}
							}
						}
						// ***
						let p = document.createElement('p'),
							pInnerText,
							additionallyText = document.createElement('p'),
							separator = `<p> / </p>`;
						p.classList.add('treeItem')
						p.insertAdjacentText('afterbegin', el.innerText);
						array.push(p.outerHTML);
						additionallyText.classList.add('additionallyText');
						if (activeTarget.dataset.level == '1') {
							additionallyText.innerText = 'Вид техники: ';
							pInnerText = newReverse(array).join(separator);
							document.querySelector('.categoryPanel__edit-tree').innerHTML = '';
							document.querySelector('.categoryPanel__edit-tree').insertAdjacentHTML('afterbegin', pInnerText);
							document.querySelector('.categoryPanel__edit-tree').insertAdjacentElement('afterbegin', additionallyText);
							document.querySelector('.categoryPanel__edit-tree').lastElementChild.classList.add('active');
							document.querySelector('.categoryPanel__edit').style.display = 'block';
						} else {
							additionallyText.innerText = 'Категория: ';
							pInnerText = newReverse(array).join(separator);
							document.querySelector('.categoryPanel__edit-tree').innerHTML = '';
							document.querySelector('.categoryPanel__edit-tree').insertAdjacentHTML('afterbegin', pInnerText);
							document.querySelector('.categoryPanel__edit-tree').insertAdjacentElement('afterbegin', additionallyText);
							for (let i = 0; i < document.querySelector('.categoryPanel__edit-tree').children.length; i++) {
								if (document.querySelector('.categoryPanel__edit-tree').children[i].className == 'treeItem') {
									document.querySelector('.categoryPanel__edit-tree').lastElementChild.classList.add('active');
								}
							}
						};
					};
				};
			};
			// вывод + вставка разделителя между элементами
			let pInnerText = newReverse(array).join(' / ');
			outputTreeContainer.innerHTML = '';
			outputTreeContainer.insertAdjacentHTML('afterbegin', pInnerText);
			return i;
		};
		// переворот массива для нормального отображения
		function newReverse(arr) {
			let a = [];
			for (i = 0; i < arr.length; i++) {
				a[i] = arr[(arr.length - 1) - i];
			};
			return a;
		};
		// находит по пути ids неообходимую часть данных в массиве
		function findDataByIds(data, ids) {
			for (var i of ids.split("-")) {
				if (!data) return null;
				if (data.sub && data.sub[i]) data = data.sub[i];
				else data = data[i];
			};
			return data;
		};
		function buldNode(parent, parentPath, data, nodeid) {
			var div = document.createElement("div");
			div.id = "ctgList-node-" + nodeid;
			div.classList.add("ctgList-itemContainer");
			div.dataset.id = nodeid;
			div.dataset.ids = parentPath ? parentPath + "-" + nodeid : nodeid;
			// добавление id уровня для узлов
			div.dataset.level = parent.dataset.level ? parseInt(parent.dataset.level) + 1 : 1;
			div.innerText = data.name;
			parent.appendChild(div);
		};
		function buldChildren(parent, data) {
			var id = parent.id + "-c"; // children
			var e = document.getElementById(id);
			if (e) {
				return e;
			}; // уже существует
			if (!data.sub) {
				return;
			}; // нет следующего уровня
			var div = document.createElement("div"); // контейнер для всех дочерних элементов
			div.id = id;
			div.classList.add("ctgList-itemContainer");
			div = parent.insertAdjacentElement('afterEnd', div);
			div.dataset.level = parent.dataset.level;
			for (id in data.sub) {
				buldNode(div, parent.dataset.ids, data.sub[id], id);
			};
			for (let i = 0; i < div.childNodes.length; i++) {
				div.style.padding = '0 0 0 30px';
				div.style.display = 'block';
			};
		};
		// создание предупреждающего окна
		function createWarningPoup(div, place, text) {
			divCreate = document.createElement("div"); // создаем див
			divCreate.classList.add("warnings"); // даем ему классы
			// разметка для элемента создать
			let divCreateInnerHtml = `
				<div class="warnings__content">
					<div class="warnings__inner">
						<div class="warnings__title">
							Внимание!!!
						</div>
						<div class="warnings__text">
							${text}
						</div>
						<div class="warnings__wrapper">
							<button class="warnings__btn" id="acceptBtn">Подтверждаю</button>
							<button class="warnings__btn" id="closeBtn">Закрыть</button>
						</div>
					</div>
				</div>
			`;
			divCreate.insertAdjacentHTML("beforeend", divCreateInnerHtml); // вставляем разметку
			divCreate = div.insertAdjacentElement(`${place}`, divCreate); // вставляем готовый блок
			setTimeout(() => {
				divCreate.querySelector('.warnings__content').style.transform = 'scale(1)';
			});
			divCreate.querySelector('#acceptBtn').addEventListener('click', () => {
				btnLockLeftPanel.classList.remove('lock');
				btnLockLeftPanel.classList.add('unlock');
				divCreate.remove();
				document.body.style.overflowY = '';
			});
			divCreate.querySelector('#closeBtn').addEventListener('click', () => {
				divCreate.remove();
				document.body.style.overflowY = '';
			});
		};
		// начальный вывод первого уровня
		for (id in ctgArray) {
			buldNode(outputCtgList, null, ctgArray[id], id);
		};
		// обработчик кликов целиком на главном элементе
		outputCtgList.addEventListener('click', onClickCtgList);
	};
	// функционал аккордеона, клики, открытие, раскрывающиеся списки, загрузка фото
	function accordionsFunctional() {
		const accordions = document.querySelectorAll('.accordion'),
			dropdowns = document.querySelectorAll('.dropdown');
		dropdowns.forEach(dropdown => {
			const select = dropdown.querySelector('.select'),
				menu = dropdown.querySelector('.menu'),
				caret = dropdown.querySelector('.caret'),
				options = dropdown.querySelectorAll('.menu li'),
				selected = dropdown.querySelector('.selected');
			if (selected.innerText != '') {
				select.classList.add('select-active');
			}
			select.addEventListener('click', (e) => {
				select.classList.toggle('select-clicked');
				caret.classList.toggle('caret-rotate');
				menu.classList.toggle('menu-open');
			});
			options.forEach(option => {
				option.addEventListener('click', (e) => {
					selected.innerText = option.innerText;
					select.classList.add('select-active');
					select.classList.remove('select-clicked');
					caret.classList.remove('caret-rotate');
					menu.classList.remove('menu-open');
					for (let i = 0; i < options.length; i++) {
						options[i].classList.remove('active');
					}
					option.classList.add('active')
				});
			});
			document.addEventListener('click', (e) => {
				if (menu.classList.contains('menu-open') && !menu.contains(e.target) && !select.contains(e.target)) {
					select.classList.remove('select-clicked');
					caret.classList.remove('caret-rotate');
					menu.classList.remove('menu-open');
				}
			});
		});
		accordions.forEach(accordion => {
			const accordionTarget = accordion.querySelector('.accordion__target'),
				accordionContent = accordion.querySelector('.accordion__inner'),
				accordionDecompression = accordion.querySelector('.accordion__decompression'),
				accordionUl = accordion.querySelector('ul'),
				accordionLi = accordion.querySelectorAll('.accordion__list li'),
				accordionMenu = accordion.querySelector('.accordion__menu');
			// добавил эту функцию из-за того, что, когда пользователь меняет внутри элементы,
			// то они могут съехать вниз без расширения самого аккордеона, поэтому она это фиксит
			function autoHeightAccordion() {
				accordionContent.style.maxHeight = accordionContent.scrollHeight + 'px';
			};
			// функция открытия аккордеона
			function openAccordion() {
				if (!accordionContent.classList.contains('accordion__inner-active')) {
					accordionContent.classList.add('accordion__inner-active');
					autoHeightAccordion();
					accordionDecompression.classList.add('active');
				} else {
					accordionContent.classList.remove('accordion__inner-active');
					accordionContent.style.maxHeight = '0';
					accordionDecompression.classList.remove('active');
				}
			};
			// клик по самому аккордеону
			accordionTarget.addEventListener('click', (e) => {
				if (e.target === accordionTarget) {
					openAccordion();
				};
			});
			// клик по правой кнопке развертывания аккордеона 
			accordionDecompression.addEventListener('click', (e) => {
				if (accordionDecompression.contains(e.target)) {
					openAccordion();
				};
			});
			// изменение внутренних пунктов в аккордеоне по клику
			function changeElementsAccordion() {
				accordionLi.forEach((el, index) => {
					let targetName = el.querySelector('.accordion__name'),
						targetComment = el.querySelector('.accordion__comment');
					if (targetName) {
						targetName.addEventListener('click', targetPropety);
					};
					if (targetComment) {
						targetComment.addEventListener('click', targetPropety);
					};
					function targetPropety(e) {
						if (count2 > 0) {
							return false;
						}
						else if (!el.classList.contains('active')) {
							count2++;
							let clonedDiv = el.cloneNode(true), // клонируем предыдущее состояние
								clonedDivImg = clonedDiv.querySelector('.accordion__list-img'),
								valueName = clonedDiv.querySelector('.accordion__name'),
								valueComment = clonedDiv.querySelector('.accordion__comment'),
								file,
								// внутренняя разметка
								innerDiv = `
									<div class="creates changed-item__photo">
										<input type="file" hidden>
										<img class="uploadImgButton" src="../img/icons/uploadImgButton.svg" alt="uploadImgButton">
										<input class="creates__input changed-item__name" type="text" placeholder="Название">
										<button class="creates__button changed-item__accept acceptImgName" type="button">ОК</button>
									</div>
									<div class="creates changed-item__comments">
										<input class="creates__input changed-item__comment" type="text" placeholder="Комментарий">
										<button class="creates__button changed-item__accept acceptComment" type="button">ОК</button>
									</div>
							`;
							el.innerHTML = ''; // очищаем внутренний текст
							el.classList.add('active'); // добавляем класс
							el.insertAdjacentHTML('beforeend', innerDiv); // вставляем разметку
							autoHeightAccordion(); // после вставки нужно обновить размер аккордеона

							const inputName = el.querySelector('.changed-item__name'),
								inputComment = el.querySelector('.changed-item__comment'),
								inputFile = el.querySelector('input[type="file"]'),
								buttonUploadImg = el.querySelector('.uploadImgButton'),
								buttonAcceptImgName = el.querySelector('.acceptImgName'),
								buttonAcceptComment = el.querySelector('.acceptComment'),
								uploadTextComment = clonedDiv.querySelector('.uploadTextComment');
							// передача фокуса тому ипнуту, чье свойство совпадает
							if (e.target == targetName) {
								inputName.focus(); // фокусируется название
								autoHeightAccordion();
							} else if (e.target == targetComment) {
								inputComment.focus(); // фокусируется комментарий
								autoHeightAccordion();
							};
							if (valueName && valueName.innerText != '') {
								inputName.value = valueName.innerText; // замена текста тем, что стоял раньше
							};
							if (valueComment && valueName.innerText != '') {
								inputComment.value = valueComment.innerText; // замена текста тем, что стоял раньше
							};
							if (clonedDivImg != null) {
								buttonUploadImg.src = clonedDivImg.src;
								buttonUploadImg.classList.add('accordion__list-img');
							};
							// клик на кнопку загрузки фотографии
							buttonUploadImg.addEventListener('click', () => {
								inputFile.click();
							});
							// изменение фотографии
							inputFile.addEventListener('change', () => {
								file = inputFile.files[0];
								showFile();
							});
							// получение данных о фото и его загрузка
							function showFile() {
								let fileType,
									validExtensions = ["image/jpeg", "image/jpg", "image/png"];
								if (file != undefined) {
									fileType = file.type;
								};
								if (validExtensions.includes(fileType)) {
									let fileReader = new FileReader();
									fileReader.onload = () => {
										let fileURL = fileReader.result;
										buttonUploadImg.src = fileURL;
										buttonUploadImg.classList.add('accordion__list-img')
										buttonUploadImg.style.margin = '0px';
										buttonUploadImg.classList.add('uploaded');
										buttonUploadImg.dataset.source = fileURL;
									};
									fileReader.readAsDataURL(file);
								};
							};
							function acceptImgName() {
								if (inputName.value != '') {
									let createImg = document.createElement('img');
									createImg.classList.add('accordion__list-img');
									valueName.innerText = inputName.value;
									let createComment = document.createElement('p');
									createComment.classList.add('accordion__comment');
									if (valueComment) {
										valueComment.innerText = inputComment.value;
									}
									el.classList.remove('active');
									let newNode = document.importNode(clonedDiv, true);
									el.innerHTML = '';
									while (newNode.children.length > 0) {
										el.appendChild(newNode.children[0]);
										autoHeightAccordion();
									}
									if (!valueComment) {
										createComment.innerText = inputComment.value;
										el.appendChild(createComment)
										autoHeightAccordion();
									}
									if (buttonUploadImg.dataset.source != undefined) {
										const imgAll = el.querySelectorAll('.accordion__list-img');
										for (let i = 0; i < imgAll.length; i++) {
											imgAll[i].remove()
										}
										createImg.src = buttonUploadImg.src;
										el.querySelector('.accordion__name').insertAdjacentElement('afterbegin', createImg);
										autoHeightAccordion();
									} else if (clonedDivImg != null) {
										const imgAll = el.querySelectorAll('.accordion__list-img');
										for (let i = 0; i < imgAll.length; i++) {
											imgAll[i].remove()
										}
										createImg.src = clonedDivImg.src;
										el.querySelector('.accordion__name').insertAdjacentElement('afterbegin', createImg);
										autoHeightAccordion();
									};
									buttonUploadImg.classList.remove('uploaded');
									count2 = 0;
									changeElementsAccordion();
								};
							};
							buttonAcceptImgName.addEventListener('click', acceptImgName);
							buttonAcceptComment.addEventListener('click', acceptImgName);
						};
					};
				});
			};
			changeElementsAccordion();
			accordionMenu.addEventListener('click', () => {
				openMenuAccordion(accordionMenu, accordionTarget);
			});
			document.addEventListener('click', (e) => {
				// !accordionTarget.querySelector('.target__accordion-menu').contains(e.target)
				if (accordionMenu.classList.contains('active') && !accordionMenu.contains(e.target) && !accordionTarget.querySelector('.target__accordion-menu').contains(e.target)) {
					for (let i = 0; i < document.querySelectorAll('.accordion__menu').length; i++) {
						document.querySelectorAll('.accordion__menu')[i].classList.remove('active')
					}
					for (let i = 0; i < document.querySelectorAll('.accordion__target .target__accordion-menu').length; i++) {
						document.querySelectorAll('.accordion__target .target__accordion-menu')[i].classList.remove('menu-open');
						document.querySelectorAll('.accordion__target .target__accordion-menu')[i].remove();
					}
				}
			});
			document.addEventListener('mousedown', (e) => {
				// !accordionTarget.querySelector('.target__accordion-menu').contains(e.target)
				if (accordionMenu.classList.contains('active') && !accordionMenu.contains(e.target) && !accordionTarget.querySelector('.target__accordion-menu').contains(e.target)) {
					for (let i = 0; i < document.querySelectorAll('.accordion__menu').length; i++) {
						document.querySelectorAll('.accordion__menu')[i].classList.remove('active')
					}
					for (let i = 0; i < document.querySelectorAll('.accordion__target .target__accordion-menu').length; i++) {
						document.querySelectorAll('.accordion__target .target__accordion-menu')[i].classList.remove('menu-open');
						document.querySelectorAll('.accordion__target .target__accordion-menu')[i].remove();
					}
				}
			});
		});
		// открытие меню аккордеона
		function openMenuAccordion(accordionMenu, accordionTarget) {
			const div = document.createElement('ul'); // создание меню
			div.classList.add('target__accordion-menu'); // добавление класса
			div.style.cssText = `
				max-width: 200px;
				left: auto;
				right: 0;
			`;
			// внутренняя оболочка
			let innerDiv = ` 
						<li id="edit">
							Редактировать
						</li>
						<li id="del">
							Удалить
						</li>
						<li id="moving">
							Переместить
						</li>
			`;
			div.insertAdjacentHTML('afterbegin', innerDiv); // вставка внутренней разметки в наш созданный див
			// проверка условия
			if (!accordionMenu.classList.contains('active') && !accordionTarget.querySelector('.input-individual-label')) {
				for (let i = 0; i < document.querySelectorAll('.accordion__menu').length; i++) {
					document.querySelectorAll('.accordion__menu')[i].classList.remove('active')
				}
				for (let i = 0; i < document.querySelectorAll('.accordion__target .target__accordion-menu').length; i++) {
					document.querySelectorAll('.accordion__target .target__accordion-menu')[i].classList.remove('menu-open');
					document.querySelectorAll('.accordion__target .target__accordion-menu')[i].remove();
				}
				accordionMenu.classList.add('active');
				accordionTarget.insertAdjacentElement('afterbegin', div); // вставка меню в блок
				setTimeout(() => {
					accordionTarget.querySelector('.target__accordion-menu').classList.add('menu-open');
				});
				// добавление функций внутренним кнопкам
				const editButton = accordionTarget.querySelector('.target__accordion-menu #edit'),
					deleteButton = accordionTarget.querySelector('.target__accordion-menu #del'),
					movingButton = accordionTarget.querySelector('.target__accordion-menu #moving');
				editButton.addEventListener('click', () => {
					editTitleAccordion(accordionTarget);
				});
			} else if (accordionMenu.classList.contains('active')) {
				delMenuAccordion(accordionMenu, accordionTarget);
			};
		};
		function delMenuAccordion(accordionMenu, accordionTarget) {
			accordionMenu.classList.remove('active');
			accordionTarget.querySelector('.target__accordion-menu').classList.remove('menu-open');
			setTimeout(() => {
				accordionTarget.querySelector('.target__accordion-menu').remove();
			}, 100);
		};
		function editTitleAccordion(accordionTarget) {
			let parent = accordionTarget,
				parentTitle = parent.querySelector('.accordion__title'),
				createLabel = document.createElement('label'),
				createInput = document.createElement('input'),
				clone = parentTitle.cloneNode(true);

			createLabel.classList.add('input-individual-label', 'white');
			createInput.classList.add('input-individual');

			createLabel.dataset.placeholder = 'Название';
			createInput.value = parentTitle.innerText;
			createInput.setAttribute('type', 'text')

			createLabel.insertAdjacentElement('afterbegin', createInput);
			parent.replaceChild(createLabel, parentTitle);
			delMenuAccordion(parent.querySelector('.accordion__menu '), parent);

			const inputs = document.querySelectorAll('.input-individual');
			inputs.forEach(input => {
				input.addEventListener('focus', () => {
					input.parentElement.classList.add('focused');
				});
				input.addEventListener('keydown', (e) => {
					if (e.keyCode === 13 && input.value != '') {
						input.blur();
					};
				});
				input.addEventListener('blur', () => {
					if (input.value != '') {
						input.parentElement.classList.add('filled');
						input.parentElement.classList.remove('focused');
						clone.innerText = input.value;
						parent.replaceChild(clone, createLabel);
						document.removeEventListener("click", handler, true);
						document.removeEventListener("mousemove", handler, true);
					} else {
						document.addEventListener("click", handler, true);
						document.addEventListener("mousemove", handler, true);
						input.focus();
						input.parentElement.classList.remove('filled');
					}
				});
			});

			createInput.focus();
		}

		function handler(e) {
			e.stopPropagation();
			e.preventDefault();
		}
		// draggable
		const list = document.querySelectorAll('.accordion');
		let draggingEle;
		let placeholder;
		let isDraggingStarted = false;
		// The current position of mouse relative to the dragging element
		let x = 0;
		let y = 0;
		// Swap two nodes
		const swap = function (nodeA, nodeB) {
			const parentA = nodeA.parentNode;
			const siblingA = nodeA.nextSibling === nodeB ? nodeA : nodeA.nextSibling;

			// Move `nodeA` to before the `nodeB`
			nodeB.parentNode.insertBefore(nodeA, nodeB);

			// Move `nodeB` to before the sibling of `nodeA`
			parentA.insertBefore(nodeB, siblingA);
		};
		// Check if `nodeA` is above `nodeB`
		const isAbove = function (nodeA, nodeB) {
			// Get the bounding rectangle of nodes
			const rectA = nodeA.getBoundingClientRect();
			const rectB = nodeB.getBoundingClientRect();

			return rectA.top + rectA.height / 2 < rectB.top + rectB.height / 2;
		};
		const mouseDownHandler = function (e) {
			let currentParent = e.target.parentNode;
			while (currentParent) {
				currentParent = currentParent.parentNode;
				if (currentParent.classList.contains('accordion')) {
					draggingEle = currentParent;
					break;
				};
			};

			// Calculate the mouse position
			const rect = draggingEle.getBoundingClientRect();
			x = e.pageX - rect.left;
			y = e.pageY - rect.top;

			// Attach the listeners to `document`
			document.addEventListener('mousemove', mouseMoveHandler);
			document.addEventListener('mouseup', mouseUpHandler);
		};
		const mouseMoveHandler = function (e) {
			const draggingRect = draggingEle.getBoundingClientRect();

			if (!isDraggingStarted) {
				isDraggingStarted = true;

				// Let the placeholder take the height of dragging element
				// So the next element won't move up
				placeholder = document.createElement('div');
				placeholder.classList.add('placeholder');
				placeholder.classList.add('accordion');
				draggingEle.parentNode.insertBefore(placeholder, draggingEle.nextSibling);
				placeholder.style.height = `${draggingRect.height}px`;
			}

			// Set position for dragging element
			draggingEle.style.position = 'absolute';
			draggingEle.style.width = placeholder.offsetWidth + 'px'
			draggingEle.style.top = `${e.pageY - 30}px`;
			draggingEle.style.left = `${e.pageX}px`;
			draggingEle.style.zIndex = '10';
			draggingEle.classList.add('drag-opacity');

			// The current order
			// prevEle
			// draggingEle
			// placeholder
			// nextEle
			const prevEle = draggingEle.previousElementSibling;
			const nextEle = placeholder.nextElementSibling;

			// The dragging element is above the previous element
			// User moves the dragging element to the top
			if (prevEle && isAbove(draggingEle, prevEle)) {
				// The current order    -> The new order
				// prevEle              -> placeholder
				// draggingEle          -> draggingEle
				// placeholder          -> prevEle
				swap(placeholder, draggingEle);
				swap(placeholder, prevEle);
				return;
			}

			// The dragging element is below the next element
			// User moves the dragging element to the bottom
			if (nextEle && isAbove(nextEle, draggingEle)) {
				// The current order    -> The new order
				// draggingEle          -> nextEle
				// placeholder          -> placeholder
				// nextEle              -> draggingEle
				swap(nextEle, placeholder);
				swap(nextEle, draggingEle);
			}
		};
		const mouseUpHandler = function () {
			// Remove the placeholder
			if (placeholder != undefined) {
				placeholder.remove();
			}

			draggingEle.style.removeProperty('top');
			draggingEle.style.removeProperty('left');
			draggingEle.style.removeProperty('position');

			x = null;
			y = null;
			draggingEle.classList.remove('drag-opacity');
			draggingEle.style.width = '100%';
			draggingEle = null;
			isDraggingStarted = false;

			// Remove the handlers of `mousemove` and `mouseup`
			document.removeEventListener('mousemove', mouseMoveHandler);
			document.removeEventListener('mouseup', mouseUpHandler);
		};
		// Query all items
		list.forEach(el => {
			el.querySelector('.accordion__draggable').addEventListener('mousedown', mouseDownHandler);
		});
	};
	// вывод ошибки для текстового поля с кнопкой создать
	function outputError(text, element) {
		let innerHtml = `
			<div class="message">
				${text}
			</div>
		`;
		if (element.nextElementSibling != null) {
			element.nextElementSibling.remove();
		}
		element.insertAdjacentHTML('afterend', innerHtml);
	};
	function deleteError(element) {
		if (element.parentElement.querySelector('.message')) {
			element.parentElement.querySelector('.message').remove();
		};
	};
	// обработчик клика для создания синонима
	let sinonims = [];
	function createSinonim() {
		outputSinonims.querySelectorAll('.sinonim').forEach(outputSinonim => outputSinonim.remove());
		sinonims.slice().reverse().forEach(sinonim => {
			const div = document.createElement('div'),
				btnClose = document.createElement('div');
			div.classList.add('sinonim');
			div.innerText = sinonim;
			btnClose.classList.add('sinonim__close');
			btnClose.addEventListener('click', () => {
				removeSinonim(btnClose, sinonim);
			});
			div.appendChild(btnClose);
			outputSinonims.insertAdjacentElement('afterbegin', div);
		});
	};
	function removeSinonim(element, sinonim) {
		let index = sinonims.indexOf(sinonim);
		sinonims = [...sinonims.slice(0, index), ...sinonims.slice(index + 1)];
		element.parentElement.remove();
	};
	function addSinonim(e) {
		if (e.target == createSinonimBtn && createSinonimInput.value != '' || e.key == 'Enter' && createSinonimInput.value != '') {
			let sinonim = createSinonimInput.value.replace(/\s*,\s*/g, ",").trim(); // удаление пробелов
			if (!sinonims.includes(sinonim)) {
				sinonim.split(',').forEach(sinonim => {
					if (!sinonims.includes(sinonim)) {
						sinonims.push(sinonim);
						createSinonim();
						deleteError(createSinonimBtn);
					} else {
						outputError('Синоним(ы) уже существуют', createSinonimBtn);
					};
				});
			} else {
				outputError('Синоним(ы) уже существуют', createSinonimBtn);
			};
			createSinonimInput.value = '';
		} else if (e.target == createSinonimBtn && createSinonimInput.value == '' || e.key == 'Enter' && createSinonimInput.value == '') {
			outputError('Вы не заполнили поле ввода', createSinonimBtn);
		};
	};
	if (createSinonimBtn && createSinonimInput) {
		createSinonimBtn.addEventListener('click', addSinonim);
		createSinonimInput.addEventListener('keyup', addSinonim);
	};
});