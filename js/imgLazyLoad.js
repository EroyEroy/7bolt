const allImg = document.querySelectorAll('img');
function allImgLazyLoad() {
	allImg.forEach(item => {
		item.setAttribute('loading', 'lazy');
	});
};
allImgLazyLoad();