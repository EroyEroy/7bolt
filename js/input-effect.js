window.addEventListener('load', () => {
	const inputs = document.querySelectorAll('.input-individual');
	inputs.forEach(input => {
		input.addEventListener('focus', () => {
			input.parentElement.classList.add('focused');
		})
		input.addEventListener('blur', () => {
			if (input.value != '') {
				input.parentElement.classList.add('focused');
			} else {
				input.parentElement.classList.remove('focused');
			}
		})
	});
});